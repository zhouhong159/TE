
&emsp;


--- - --

&#x1F34F; ${\color{red}{NEWs！}}$ 

- &#x2753; 课程答疑和 FAQs: [「 Wikis」](https://gitee.com/arlionn/TE/wikis/Home) 
- &#x1F353; 课程详情：[「效率专题-TE」](https://gitee.com/arlionn/TE/blob/master/Outline.md) &#x1F353;
- 参考文献：[PDFs](https://quqi.gblhgk.com/s/880197/gpBOtFxCoWjzRMnL)


--- - --

&emsp;

&emsp;

> #### [连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)     
> &#x1F34E; **视频**：已上线，可随时购买学习+全套课件    
> &#x1F4D7; 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> [课程主页](https://gitee.com/arlionn/TE)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg)，[PDF版](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TE专题_连享会_lianxh.cn700.png "连享会-效率分析专题视频，三天课程，随时学")


&emsp;

> Stata连享会 &ensp;   [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.zhihu.com/people/arlionn/) 




&emsp;


## [课程概览](https://gitee.com/arlionn/TE/blob/master/Outline.md) 

- **简介：** 本课程通过三个专题的讲解，系统介绍各类效率分析工具的原理和 Stata 实现方法，包括： **TFP  专题** (全要素生产率)、**DEA 专题** (数据包络分析)、**SFA 专题** (随机边界分析)。每个主题提供 1-2 篇论文的讲解，附带完整的重现文件，以便大家能应用于自己的实证分析之中。
- **授课方式：网络直播** (15 天回放)，缴费后主办方发送邀请码点击邀请码即可进入在线课堂，收看直播无需安装任何软件。
- 时间：2020 年 5 月 29-31 日 (周五-周日)
  - **时段：** 上午 9:00-12:00; 下午 2:30-5:30；课后 30-60 分钟答疑
- **授课嘉宾：**
  - 鲁晓东 (中山大学)
  - 张 &ensp; 宁 (暨南大学)
  - 连玉君 (中山大学)
- **软件/课件：** Stata 软件，提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)
- **课程主页** &#x1F353; [-- 点此查看课程详情 --](https://gitee.com/arlionn/TE/blob/master/Outline.md) &#x1F353;
- **报名链接：** <http://junquan18903405450.mikecrm.com/FhWLhS2>


#### 参考文献 PDF 下载
课程大纲中涉及的所有参考文献可以直接在下方对应课程介绍中点击链接打开，亦可到百度云盘打包下载：
- 链接：https://pan.baidu.com/s/1DPsrmaaTHLvMK2PifEMYug 
- 提取码：hll8 


## 报名和缴费信息 [「点我查看」](https://gitee.com/arlionn/TE/blob/master/Outline.md) 

&emsp;

&emsp;

## 相关课程 


&emsp; 

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| **[效率分析-专题](https://gitee.com/arlionn/TE)** | 连玉君<br>鲁晓东<br>张&emsp;宁 | [视频-TFP-SFA-DEA](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线 |
| **文本分析/爬虫** | 游万海<br>司继春 | [视频-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线 |
| **结构方程模型-SEM** | 阳义南 | [直播：结构方程模型及Stata应用](https://lianxh.duanshu.com/#/brief/course/6cc4caad765346cfa19c8f4f48a51ace) |
| **二元选择模型** | 司继春 | [直播：二元选择与计数数据](https://www.lianxh.cn/news/91c3fc21cdbbc.html) |
| **分位数回归** | 游万海 | [视频，2小时，88元](https://lianxh.duanshu.com/#/brief/course/f0bfb3102ada48969966c92123a7ebf0) |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 论文重现 | 连玉君    | [经典论文精讲](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，[-课程资料-](https://gitee.com/arlionn/Paper101), [-幻灯片-](https://quqi.com/s/880197/nz9LvbzzEEpWBNrD)   |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |
| 大数据 | 李兵 | [经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26) |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">



&emsp;

> #### [连享会 - Stata 暑期班](https://gitee.com/arlionn/PX)     
> **线上直播 9 天**：2020.7.28-8.7  
> **主讲嘉宾**：连玉君 (中山大学) | 江艇 (中国人民大学)      
> **课程主页**：<https://gitee.com/arlionn/PX> | [微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2020Stata暑期班海报750.png "连享会：Stata暑期班，9天直播")

&emsp; 


&emsp;

> #### [连享会 - 文本分析与爬虫 - 专题视频](https://www.lianxh.cn/news/88426b2faeea8.html)    
> 主讲嘉宾：司继春 || 游万海

![连享会-文本分析与爬虫-专题视频教程](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lanNew-文本分析-海报002.png "连享会-文本分析与爬虫-专题视频，四天课程，随随时学")

